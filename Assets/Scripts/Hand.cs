﻿using UnityEngine;
using System.Collections;

public class Hand : MonoBehaviour {
	public GameObject hand;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject[] objs = GameObject.FindGameObjectsWithTag ("Spawnpoint");
		if (hand != null) {
			//Smooth object motion
			hand.transform.position = Vector3.Lerp (hand.transform.position, 
			                                        this.transform.position + transform.forward + 0.75f * transform.right - 0.75f * transform.up, 0.3f);
			hand.transform.rotation = Quaternion.Lerp(hand.transform.rotation, this.transform.rotation, 0.3f);

			////For Non-Smooth Motion////
			//hand.transform.position = this.transform.position + transform.forward + 0.75f * transform.right - 0.75f * transform.up;
			//hand.transform.rotation = this.transform.rotation;
		}

		if (Input.GetButtonDown("Pickup"))
			pickup();
		if (Input.GetButtonDown("Drop"))
			drop ();
		
		if ((Input.GetButton("Activate") || Input.GetButton("AltActivate")) && hand != null) {
			Holdable child = hand.GetComponent<Holdable>();
			child.Activate();
		}
	}
	
	void pickup() {
		GameObject[] objs = GameObject.FindGameObjectsWithTag ("Holdable");						// Only "holdable" tagges objects will be searched (saves time)
		float dist = Mathf.Infinity;
		GameObject closest = null;
		foreach (GameObject obj in objs) {
			if (obj.GetComponent<Holdable> ().held == false){									// if it's not currently being held (i.e. the thing you're holding or an enemy's gun)
				float ndist = Vector3.Distance (obj.transform.position, transform.position);	
				if ( ndist < 3.0f && ndist < dist) {											// if it's close enough and closer than the current closest thing
					closest = obj;
					dist = ndist;
				}
			}
		}

		if (closest != null) {								// If something actually gets found
			if (hand != null)								// If already holding something
				hand.GetComponent<Holdable>().Drop();		// drop it like its hot
			hand = closest;									// pickup object (hand guanenteed to be null here
			closest.GetComponent<Holdable> ().Pickup();		// call pickup logic
		}

		//// for dualwielding logic ////
		//if (right == null && left == null)
		//		right = closest;
		//	holding.transform.parent = transform;
	}
	
	void drop(){
		if (hand != null)
			hand.GetComponent<Holdable> ().Drop ();
		hand = null;
	}
}
