﻿using UnityEngine;
using System.Collections;

public class SampleHoldable : Holdable {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void Activate() {				// very important to have "override" keyword in method declaration
		Debug.Log (gameObject.name + " activated");
		if (Input.GetButtonDown("Activate"))
			light.enabled = !light.enabled;			// also make sure components you reference actually exist in the object
	}
}
