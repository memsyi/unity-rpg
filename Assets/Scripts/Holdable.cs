﻿using UnityEngine;
using System.Collections;

public abstract class Holdable : MonoBehaviour {
	public bool dualwieldable;
	public bool held = false;
	
	// Use this for initialization
	void Start () {
		try {
			foreach (GameObject go in GameObject.FindGameObjectsWithTag("MainCamera")) {
				Physics.IgnoreCollision (go.collider, collider);
				Physics.IgnoreCollision (go.GetComponent<CharacterController> ().collider, collider);
			}
		} catch (MissingComponentException mce){
			string s = mce.StackTrace; //Ignore this, it's okay
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	// Proper way to pick up item
	public void Pickup() {	// "soft removal" from game world
		rigidbody.velocity = new Vector3 (0, 0, 0);			// zero out velocities to avoid ugly flickering bug
		rigidbody.angularVelocity = new Vector3 (0, 0, 0);
		collider.enabled = false;							// don't collide with walls etc
		held = true;										// actually is held
		rigidbody.useGravity = false;						// avoid drifting while in hand
		
	}
	
	// Proper way to drop item
	public void Drop() {	//re-enable forces etc on object 								
		collider.enabled = true;
		held = false;
		rigidbody.useGravity = true;
	}
	
	// Proper way to call activation
	public abstract void Activate ();
}