﻿using UnityEngine;
using System.Collections;

public class ObjectivesListener : MonoBehaviour {
	public ObjectivesList list;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void addSimpleObjective(string shortName, string displayText){
		list.addSimpleObjective(shortName, displayText);
	}
	
	public void addComplexObjective(string shortName, string displayText, string[] bullets){
		list.addComplexObjective(shortName, displayText, bullets);
	}
	
	public void completeObjective(string name){
		list.completeObjective(name);
	}
}
