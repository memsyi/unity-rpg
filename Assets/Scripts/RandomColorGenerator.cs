﻿using UnityEngine;
using System.Collections;

public class RandomColorGenerator : MonoBehaviour {

	[RPC]
	void SetColor(Vector3 colorVector) {
		Color color = new Color(colorVector.x, colorVector.y, colorVector.z);
		SkinnedMeshRenderer mySkin = this.transform.GetComponentInChildren<SkinnedMeshRenderer>();
		mySkin.material.color = color;
	}
}
