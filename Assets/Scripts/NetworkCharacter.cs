﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	string realName = "";

	Animator anim;

	// Use this for initialization
	void Start () {
		CacheComponents();
	}

	void CacheComponents() {
		if(anim == null) {
			anim = GetComponent<Animator>();
			if(anim == null) {
				Debug.LogError ("You forgot to put an Animator component on this character prefab!");
			}
		}
		
		// Cache more components here if required!
	}

	// Update is called once per frame
	void Update () {
		if (!photonView.isMine) {
			transform.position = Vector3.Lerp (transform.position, realPosition, 0.1f);
			transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, 0.1f);
			transform.name = realName;
		}
	}
	
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if(stream.isWriting) {
			// This is OUR player. We need to send our actual position to the network.
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(transform.name);
			stream.SendNext(anim.GetFloat("Speed"));
			stream.SendNext(anim.GetBool("Jumping"));
			stream.SendNext(anim.GetBool("Back"));
			stream.SendNext(anim.GetBool("Left"));
			stream.SendNext(anim.GetBool("Right"));
			           
		}
		else {
			// This is someone else's player. We need to receive their position (as of a few
			// millisecond ago, and update our version of that player.	
			realPosition = (Vector3)stream.ReceiveNext();
			realRotation = (Quaternion)stream.ReceiveNext();
			realName = (string)stream.ReceiveNext();
			// Weird error when sending these values but no breaking behavior
			if ( anim != null) {
				anim.SetFloat("Speed", (float)stream.ReceiveNext());
				anim.SetBool("Jumping", (bool)stream.ReceiveNext());
				anim.SetBool("Back", (bool)stream.ReceiveNext());
				anim.SetBool("Left", (bool)stream.ReceiveNext());
				anim.SetBool("Right", (bool)stream.ReceiveNext());
			}
		}
	}
}
