﻿using UnityEngine;
using System.Collections;

///////////////////////////////////
///
//////////////////////////////////////////////////////////
///// 														Make this Abstract and add elemental damages
//////////////////////////////////////////////////////////
///
//////////////////////////////////

public class DamageEntity : MonoBehaviour {
	public MagicElement element;
	public float health;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void kill ( ){
		Destroy (gameObject);
	}
	
	public void hit ( float damage ){
		health -= damage;
		if (health <= 0.0f)
			kill ();
	}

	public void elementalHit ( float damage, string damageElement) {
		float damageScale = 0.0f;
		
		//if(water && fire || fire && air || air && water)
		//		return 1.5f damage
		
		
		switch (element.name) {
		case "fire": 
			switch (damageElement) {
			case "fire": damageScale = 0.25f; break; 
			case "water":  damageScale = 1.5f; break;
			case "air":  damageScale = 0.5f; break;
			case "magi":  damageScale = 1.0f; break;
			case "lightning":  damageScale = 2.0f; break;
			} break; 
		case "water": 
			switch (damageElement) {
			case "fire": damageScale = 0.5f; break; 
			case "water":  damageScale = 0.25f;	break;
			case "air":  damageScale = 1.5f; break;
			case "magi":  damageScale = 1.0f; break;
			case "lightning":  damageScale = 2.0f; break;
			} break;
		case "air": 
			switch (damageElement) {
			case "fire": damageScale = 0.5f; break; 
			case "water":  damageScale = 1.5f; break;
			case "air":  damageScale = 0.25f; break;
			case "magi":  damageScale = 1.0f; break;
			case "lightning":  damageScale = 0.5f; break;
			} break;
		case "magi": 
			switch (damageElement) {
			case "fire": damageScale = 1.0f; break; 
			case "water":  damageScale = 1.0f; break;
			case "air":  damageScale = 1.0f; break;
			case "magi":  damageScale = 0.0f; break;
			case "lightning":  damageScale = 2.0f; break;
			} break;
		case "lightning": 
			switch (damageElement) {
			case "fire": damageScale = 0.5f; break; 
			case "water":  damageScale = 0.5f; break;
			case "air":  damageScale = 0.5f; break;
			case "magi":  damageScale = 0.5f; break;
			case "lightning":  damageScale = 0.25f; break;
			} break;
		}
		hit (damage * damageScale);
	}
}
