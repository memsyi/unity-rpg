﻿using UnityEngine;
using System.Collections;

public class ChargeGloves : Holdable {
	//serialize element for bonus / reduced damage against elemental enemies
	private MagicElement[] elements = {
		new MagicElement ("fire", new Color(1, 0.3f, 0), -1.0f), 	
		new MagicElement ("water", Color.blue, 0.5f), 
		new MagicElement ("wind", Color.white, 0.0f), 
		new MagicElement ("magi", Color.magenta, 0.0f),
		new MagicElement ("lightning", Color.yellow, 0.0f)};
	private MagicElement right;
	public ParticleSystem rightParticle;
	private MagicElement left;
	
	private float leftRadius;
	private float rightRadius;
	
	public ParticleSystem leftParticle;
	
	public MagicProjectile projectile;
	
	private MagicProjectile lprojectile;
	private MagicProjectile rprojectile;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (held) {
			if(Input.GetButton("Activate")){
				if (rightRadius == 0.0f) {
					rprojectile = Instantiate( projectile, rightParticle.transform.position, rightParticle.transform.rotation ) as MagicProjectile;
					rprojectile.setElement(right);
					rightRadius += 0.01f;
				} else {
					rightRadius += 0.01f;
					rprojectile.transform.position = rightParticle.transform.position;
					rprojectile.transform.rotation = rightParticle.transform.rotation;
					rprojectile.transform.localScale = rightRadius * Vector3.one;
				}
			} else if (!Input.GetButton("Activate") && rightRadius > 0.2f ) {
			//	rprojectile.rigidbody.velocity = 40.0f * transform.forward;
				rightRadius = 0.0f;
				//GameObject go = Instantiate(rprojectile, rightParticle.transform.position, rightParticle.transform.rotation) as GameObject;
				rprojectile.rigidbody.velocity = 40.0f * transform.forward;
			//	Destroy(rprojectile.gameObject);
			} else {
				//if(rprojectile != null) Destroy(rprojectile.gameObject);
				//rprojectile = Instantiate( projectile, rightParticle.transform.position, rightParticle.transform.rotation ) as MagicProjectile;
				rightRadius = 0.0f;
			}

			if(Input.GetButton("AltActivate")){
				if (leftRadius == 0.0f) {
					lprojectile = Instantiate( projectile, leftParticle.transform.position, leftParticle.transform.rotation ) as MagicProjectile;
					lprojectile.setElement(left);
					leftRadius += 0.01f;
				} else {
					leftRadius += 0.01f;
					lprojectile.transform.position = leftParticle.transform.position;
					lprojectile.transform.rotation = leftParticle.transform.rotation;
					lprojectile.transform.localScale = leftRadius * Vector3.one;
				}
			} else if (!Input.GetButton("AltActivate") && leftRadius > 0.2f ) {
				//	lprojectile.rigidbody.velocity = 40.0f * transform.forward;
				leftRadius = 0.0f;
				//GameObject go = Instantiate(lprojectile, leftParticle.transform.position, leftParticle.transform.rotation) as GameObject;
				lprojectile.rigidbody.velocity = 40.0f * transform.forward;
				//	Destroy(lprojectile.gameObject);
			} else {
				//if(lprojectile != null) Destroy(lprojectile.gameObject);
				//lprojectile = Instantiate( projectile, leftParticle.transform.position, leftParticle.transform.rotation ) as MagicProjectile;
				leftRadius = 0.0f;
			}
				
			//	if (rightRadius > 0.5f){
			//		mp.rigidbody.velocity = 40.0f * transform.forward;
			//		mp.transform.localScale = rightRadius * mp.transform.localScale;
			//		mp.setElement(right);
			//		rightRadius = 0.0f;
			//	} else {
			//		rightRadius = 0.0f;
			//	}
			
			
			//	if (Input.GetMouseButton (0)) {
			//		leftRadius += 0.05f;
			//do raycasting hits
			//	}
			
			if (Input.GetKeyDown (KeyCode.E)) {
				right = elements [Random.Range (0, elements.Length)];
				rightParticle.startColor = right.color;
				rightParticle.gravityModifier = right.gravityMultiplier;
			}
			
			if (Input.GetKeyDown (KeyCode.Q)) {
				left = elements [Random.Range (0, elements.Length)];
				leftParticle.startColor = left.color;
				leftParticle.gravityModifier = left.gravityMultiplier;
			}
		}
	}
	
	public override void Activate(){
		//	Debug.Log("Bloop");
		//	if (Input.GetMouseButton (1)) {
		
		//do raycasting hits
		//	}
		//	if (Input.GetMouseButton (0)) {
		//		leftParticle.enableEmission = false;
		//		//do raycasting hits
		//	}
	}
}