# ** DOCUMENTATION AND HOW TO ** #

*I know this looks daunting, but it's all pretty simple, and important to know.*  

## Your First Level: ##

* Player
* Map
* Tools and Weapons
* Quests
* Exit

### Player: ###
Every level needs a player to function properly.  Due to the way the levels are structured, there are two ways to create a player, one right, and one wrong.  

The correct way to add the player to your world so it can work properly with the context of the rest of the game is to place a memorably named `CharacterSpawner` prefab in the position you would like the character to be spawned in.  However, this is inconvenient for testing your level, so during development, you can just put a `Player` prefab where you want to spawn.  **IT IS VERY IMPORTANT THAT YOU HAVE A SPAWN POINT AND DELETE THIS TESTING PLAYER BEFORE COMMITTING AND PUSHING YOUR CODE.**  Without a `Character Spawn`, the game does not know where to put your player, and with the debugging player still in the game, the game may behave erratically. In the `Player` prefab (a set model and scripts that can be changed once and duplicated everywhere) there is only one script that matters (for our and your purposes anyway).  This is `Hand.cs`.  Hand is the script that allows the player to pick up, drop, and interact with the world. For more in-depth information, see the section on *Creating Tools and Weapons*.


### Map: ###

This is the level that you will actually be creating.  This can be a terrain created with the terrain tools, or a collection of planes and cubes to build twisting corridors.

Additionally, your level needs an exit back to the overworld (or another level!)  There is a prefab named `LevelLoader` available for you to use for this purpose.  It requires a string for the name of the level to load, and a string for the name of the spawn point to go to in that level.

### Tools and Weapons: ###

Tools and Weapons are the lifeblood of your game, the main way you interact with the world, aside from moving around in it. A framework has been provided for you to quickly and easily build objects for your world without much hassle.  They are picked up with the `F` key, and dropped with the `Z` key.  

There are three steps to complete if you want your object to work correctly:  

1. Create or Download a model  
2. Extend `Holdable.cs`, and write your script  
3. Put the object in the gameworld, tag as holdable, and add your script  

**Model!** 

In his Maya demo Sebastien showed us how to make a model from images online, or there are many sites available to download the models you need

**Code!**

This is the tricky part of the whole operation, but also where the heart of the whole game lies. `Holdable.cs` is an abstract representation of a usable object in the game.  To extend `Holdable.cs` create a new C# script and give it a name.  Open it in MonoDevelop, and change where it says `public class YourClass : MonoBehavior {` to `public class YourClass : Holdable {`  Your class is now extending the type `Holdable`, and all you have to do now is fill in the method `public override void Activate() { }` with whatever code you want to run while the object is activated.  Note that activate is called every frame that either of the mouse buttons is down, so if you only want to activate when the mouse is clicked, make sure to check for that!

**Assemble!**

Now that you have the model and the script ready, you can position and size your model in the world, and add your script as a component on the object.  Before you can use it in the world however, there are two things you need to make sure you do.  You need to add a rigidbody to your model, and tag it as `Holdable`.  Tagging something as holdable is done through a dropdown menu right below the object name in the inspector, scroll down to `Holdable` and tag it, you should be good to go!


### Quests: ###

Quests are the main way that you as the designer interact with the player, "go here" "do this" "kill this monster" are all examples (albeit quite boring) quests.  The quest system does a bit of work behind the scenes, but all you need to know is that quests have three mandatory variables, a short name, long description, and a completed boolean. You can make a quest anywhere you can get access to the list of quests, but in this tutorial, I will only cover making and completing quests from the editor. (If you do want to do quests from a script call, I'll be glad to show you, it's just a bit beyond this "quick" tutorial). 

There are two types of objects you need to be aware of.  The first is the `ObjectivesList`.  There should only ever be one of these and it holds an array of all your quests.  This is all taken care of for you, you shouldn't need to worry how it works.  

What is important is how to create and resolve quests from the editor.  Think of the short name as a variable name, something unique and memorable.  For example, if you had an objective where you had to kill an enemy, a good name for it could be `"killbob"`. add the name of your level to the beginning of the quest to keep things portable (the objectives list won't like five different quests called `"killbob"`. a better name would be `"jack-killbob"`.  The long description is the text that is actually shown to the player. for our `killbob` example, a good description could be `"Avenge your brother and kill the traitor Bob"`. Try to keep this short-ish so you don't overwrite the whole screen with your quest description.

To actually work with quests, a prefab `ObjectiveObject` is provided.  Drag and scale the object to where you want the trigger to be then fill input the quest information.

To *complete a quest*, choose `complete` from the dropdown menu and fill in the short name of the quest you want to finish when the player enters the area

To *create a simple quest*, choose `create` from the dropdown, and fill in the unique name and description of the quest to create.

To *create a multi part quest* (for example to power down the doomsday machine, you have to blow up four power nodes first) select `createComplex` from the dropdown and fill in the unique name for the collection of subquests `"jack-doomsday"` and description, then in the array that follows, put in pairs of strings with short names and descriptions of the subquests `{"jack-node1", "Power down Node 1", "jack-node2", ... }`.  this array **ABSOLUTELY MUST** be an even number.  Subquests are completed the same way as main quests, simply by the shortname of the subquest.  